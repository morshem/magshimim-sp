#include "ceaser.h"
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char shift_letter(char letter, int offset)
{
	if(letter <= 'z' && letter >= 'a')
	{
		return ((letter - 'a' + offset) % 26 + 'a');
	}
	else if(letter <= 'Z' && letter >= 'A')
	{
		return ((letter - 'A' + offset) % 26 + 'A');
	}
	else
	{
		return letter;
	}

}

char * shift_string(char * input, int offset)
{
	int length = strlen(input);
	int i;
	char* encrypted = (char*)malloc(length*sizeof(char));

	for (i = 0; i < length; ++i)
		encrypted[i] = shift_letter(input[i],offset);

	return encrypted;
}
