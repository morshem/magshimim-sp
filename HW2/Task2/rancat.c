#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main( int argc, char** argv)
{
	if(argc != 2)
	{
		printf("NAME\n\trancat - print random line\nSYNOPSIS\n\t rancat FILE\nDESCRIPTION\n\t Print a random line from the FILE on the standard output.\n");
		printf("Argc: %d", argc);
		return 1;
	}

	FILE *cattedFile =  fopen(argv[1], "r");
	srand(time(0));
	int count = 0;
	char line[1000][1000];
	while(!feof(cattedFile))
	{
		fgets(line[count], 1000, cattedFile);
		count++;
	}
	
	int randfile = rand()%count;
	printf("%d\n, %s\n\n\n\n\n",count,  line[randfile]);

	fclose(cattedFile);
	return 0;
}
