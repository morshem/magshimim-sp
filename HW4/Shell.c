#include <unistd.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LineParser.h"

void execute(cmdLine *pCmdLine);

#define GO_ON 1
#define INPUT_SIZE 2048

int main()
{
	char command[INPUT_SIZE], currDir[INPUT_SIZE];
	
	cmdLine *pCmdLine;
	
	do
	{
		syscall(SYS_getcwd, currDir, sizeof(currDir));
		printf("%s $", currDir);
		
		fflush(NULL);
		fgets(command, INPUT_SIZE, stdin);
		
		pCmdLine = parseCmdLines(command);
		
		if(strcmp(command, "quit\n") == 0 || strcmp(command, "quit") == 0)
		{
			break;
		}
		else if(strcmp(pCmdLine->arguments[0], "cd") == 0)
		{
			chdir(pCmdLine->arguments[1]);
		}
		
		execute(pCmdLine);
		
	} while(GO_ON);
}

void execute(cmdLine *pCmdLine)
{
	pid_t pid;
	pid = fork();
	if(pid == 0)// Child Process
	{
		if(strcmp(pCmdLine->arguments[0], "myecho") == 0)
			execvp("echo", pCmdLine->arguments);
		else
			execvp(pCmdLine->arguments[0], pCmdLine->arguments);
		exit(0);
	}
	
	waitpid(pid);
	
	return;
}



