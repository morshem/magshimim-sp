#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <fcntl.h> //For bonus stages
#include "helper.h"



void _myExit();
void _printID();
void _printDirectory();
char* __itoa(int num);

int myStrLen(char* str)
{
	int len = 0;
	while(str[len] != '\0')
	{
		len++;
	}
	
	return len;
}

void newLine()
{
	char* newLine = "\n\r";
	syscall(SYS_write, 0, newLine, 2);
}



int main()
{
	int choise;
	char choiseChr[2];
	char* menu = "1)Exit\n2)Print your ID\n3)Print current directory";
	
	fun_desc funcList[3];
	//initialize funcList
	{
	//Exit
	funcList[0].name = "Exit";
	funcList[0].fun = _myExit;
	
	//ID
	funcList[1].name = "ID";
	funcList[1].fun = _printID;
	
	//Dir
	funcList[2].name = "Dir";
	funcList[2].fun = _printDirectory;
	}
	
	do
	{
		//Print the options
		syscall(SYS_write, 0, menu, myStrLen(menu) + 1);
		newLine();
		
		//Recieve choise
		syscall(SYS_read, 0, choiseChr, 2);
		
		//Make it an int
		choise = (int)(choiseChr[0] - '0') - 1;
		
		//Call the fitting function
		if(choise >= 0 && choise <= 2)
		{
			(*funcList[choise].fun)();
		}
		else
		{
			return 1;
		}
	}while(1==1);
	
	return -1;//LOL what how did the code get to here? 
}

void _myExit()
{
	syscall(SYS_exit, 0);
}

void _printID()
{
	char* userID = __itoa(syscall(SYS_getuid));
	syscall(SYS_write, 0, userID, myStrLen(userID)+1);
	newLine();
}

void _printDirectory()
{
	char dir[1024];
	syscall(SYS_getcwd, dir, sizeof(dir));
	syscall(SYS_write, 0, dir, myStrLen(dir)+1);
	newLine();
}



