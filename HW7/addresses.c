#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/syscall.h>

int addr5 = 24;
int addr6;

int addr4();


int main (int argc, char** argv){
	static int addr1;
	int addr2;
	int addr3[3];
	char* addr7="?";
	int * addr8 = (int*)malloc(12);
	printf("1 -%p BSS\n",&addr1);//Static local variable, in the global memory -BSS-, referenced in each vocation
	printf("2 -%p STACK \n",&addr2);//uninitialized local variable, in the STACK
	printf("3 -%p STACK\n",&addr3);//uninitialized local variable, in the STACK
	printf("4 -%p TEXT\n",addr4);//In the stack segment, the text itself is in the TEXT segment
	printf("5 -%p DATA\n",&addr5);//He is the in DATA segment, preditermined global value
	printf("6 -%p BSS\n",&addr6);//In the BSS segment, uninitialized global variable
	printf("7 -%p TEXT\n",addr7);//Preditermined string literal, goes to the TEXT segment
	printf("8 -%p HEAP\n",addr8);//IN the HEAP segment, dynamically allocated memory
	
	printf("Type: cat /proc/%d/maps\n", syscall(SYS_getpid));
	int wait;
	scanf("%d", &wait);
	
	return 0;
}

int addr4(){
	return -1;
}



