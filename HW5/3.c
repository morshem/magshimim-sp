#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //for syscall
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <string.h>
#include <sys/stat.h> 
#include <fcntl.h>

int main()
{
	char * const lsCommand[]={"ls","-l",NULL};
	char * const tailCommand[]={"tail","-n", "2",NULL};
	int fd[2];
	int dup1;
	int child1,child2;
	pipe(fd);
	child1=fork();
	if(child1==-1)
	{
		perror("fork");
		exit(0);
	}
	if(child1==0)	//Child 1 
	{
		close(1);
		dup1=dup(fd[1]);
		close(fd[1]);
		execvp(lsCommand[0],lsCommand);
		exit(0);
	}
	else
	{
		close(fd[1]);
		wait(child1);
	}

	child2=fork();
	if(child2==-1)
	{
		perror("fork");
		exit(0);
	}
	if(child2==0)	//Child 2
	{
		close(0);
		dup1=dup(fd[0]);
		close(fd[0]);
		execvp(tailCommand[0],tailCommand);
		exit(0);
	}
	else
	{
		close(fd[0]);
		wait(child2);
	}
}
