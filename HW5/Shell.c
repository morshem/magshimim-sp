#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "LineParser.h"

#define PATH_MAX 1024

void execute(cmdLine * pCmdLine);

int main()
{
	struct cmdLine *my_cmd;
	char dir[PATH_MAX];
	char user_input[2049];
	while(1)
	{
		getcwd(dir, sizeof(dir));
		printf("\n%s " , dir);
		fgets (user_input , sizeof(user_input) , stdin);
		if(strcmp(user_input , "quit\n") == 0)
			break;
		my_cmd = parseCmdLines(user_input);
		execute(my_cmd);
	}
	return 0;
}

void execute(cmdLine * pCmdLine)
{
	int pid;
	if(strcmp(pCmdLine->arguments[0] , "cd") == 0)
	{
		if(chdir(pCmdLine->arguments[1]) == -1)
			printf("Chdir error\n");
		return;
	}
	if ((pid = fork()) == -1)
        	perror("fork error");
	else if (pid == 0)	//child proccess
        {
		if(strcmp(pCmdLine->arguments[0] , "myecho") == 0)
		{
			int i=1;
			printf("\n");
			for(;i<pCmdLine->argCount ; i++)
				printf("%s " , pCmdLine->arguments[i]);
			printf("\n");
			exit(0);
		}
		if(pCmdLine->inputRedirect)
		{
			close(0);	//close STDIN_FILENO  
			open(pCmdLine->inputRedirect,O_RDONLY);
		}
		if(pCmdLine->outputRedirect)
		{
			close(1);	//close STDOUT_FILENO
			open(pCmdLine->outputRedirect,O_WRONLY);
		}
		if(execvp(pCmdLine->arguments[0], pCmdLine->arguments)==-1) 	//Ragular command
		{
			perror("execvp");
			exit(1);
		}
		exit(0);
	}
	else	//father proccess
	{
		int status;
		if(pCmdLine->blocking == 1)
		{
			waitpid(pid , &status , 0);
		}
	}
	freeCmdLines(pCmdLine);
}

