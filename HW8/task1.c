#pragma pack(push,1)

#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  char  e_ident[16];
  short e_type;
  short e_machine;
  int e_version;
  int e_entry;
  int e_phoff;
  int e_shoff;
  int e_flags;
  short e_ehsize;
  short e_phentsize;
  short e_phnum;
  short e_shentsize;
  short e_shnum;
  short e_shstrndx;
} Elf32Hdr;

typedef struct
{
  int sh_name;
  int sh_type;
  int sh_flags;
  int sh_addr;
  int sh_offset;
  int sh_size;
  int sh_link;
  int sh_info;
  int sh_addralign;
  int sh_entsize;
} Elf32SectHdr;

#pragma pack(pop)



void main()
{
  FILE* ElfFile = NULL;
  char* SectNames = NULL;
  Elf32Hdr elfHdr;
  Elf32SectHdr sectHdr;
  int idx;

printf("Hello@!");

  ElfFile = fopen("/home/mor/Magshimim/magshimim-sp/HW8/addresses.run", "r");
  if(ElfFile == NULL)
  {
  	printf("Error opening file");
  	return;
  }

  // read ELF header
  fread(&elfHdr, 1, sizeof elfHdr, ElfFile);

  // read section name string table
  // first, read its header
  fseek(ElfFile, elfHdr.e_shoff + elfHdr.e_shstrndx * sizeof sectHdr, SEEK_SET);
  fread(&sectHdr, 1, sizeof sectHdr, ElfFile);

  // next, read the section, string data
  SectNames = malloc(sectHdr.sh_size);
  fseek(ElfFile, sectHdr.sh_offset, SEEK_SET);
  fread(SectNames, 1, sectHdr.sh_size, ElfFile);

  // read all section headers
  for (idx = 0; idx < elfHdr.e_shnum; idx++)
  {
    const char* name = "";

    fseek(ElfFile, elfHdr.e_shoff + idx * sizeof sectHdr, SEEK_SET);
    fread(&sectHdr, 1, sizeof sectHdr, ElfFile);

    // print section name
    if (sectHdr.sh_name);
      name = SectNames + sectHdr.sh_name;
    printf("%2u %s\n", idx, name);
  }

// ...
}
