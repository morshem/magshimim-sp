#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

void killChild(pid_t childpid,int signum)
{
	kill(childpid,signum);
}

int main()
{
	pid_t childpid;
	if((childpid = fork()) == -1)
        {
                perror("fork");
                exit(1);
        }

        if(childpid == 0)	//Child 
        {
		while(1)
		{
			printf("I'm still alive\n");
		}
        }
        else	//Father
        {
		sleep(1);
		printf("Dispatching\n");
		killChild(childpid,SIGKILL);
		printf("Dispatched\n");
        }	

	return 0;
}

