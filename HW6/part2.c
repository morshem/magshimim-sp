#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

void quit(int signum);

int main()
{
	int i=0;
	for(i=0;i<2000000000;i++)
	{
		signal(SIGINT,quit);	//2 : Control+C
		signal(SIGQUIT,quit);	//3 : Control+\ 
		signal(SIGTERM,quit);	//15
		if(i%1000000==0)
			printf("I'm still alive i=%d\n",i);
	}
	exit(EXIT_SUCCESS);
}


void quit(int signum)
{
	char keepGoing[1024];
	printf("Do you want to quit ? (Y\\y - yes , else - no)\nAnswer : ");
	fgets(keepGoing, sizeof(keepGoing) , stdin);

	if(keepGoing[0]=='y'||keepGoing[0]=='Y')
	{
		exit(0);
	}
	else
	{
		signal(signum,SIG_IGN);		//Ignore the signal.
	}
}

